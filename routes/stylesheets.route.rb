get '/style/:file.css' do
    base_path = File.join(File.dirname(__FILE__), '..', 'views')

    rel_path = File.join('scss', params[:file])

    if File.exists?(File.join(base_path, rel_path + '.scss'))
        begin
            scss rel_path.to_sym
        rescue
            halt 500
        end
    else
        pass
    end
end