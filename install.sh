#!/bin/bash

REPO_URL="https://bitbucket.org/hmbl9r/sinatra-boilerplate/get/master.tar.gz"

NORMALIZE_URL="http://necolas.github.io/normalize.css/3.0.1/normalize.css"
JQUERY_URL="http://code.jquery.com/jquery-latest.min.js"
UNDERSCORE_URL="http://underscorejs.org/underscore-min.js"
BACKBONE_URL="http://backbonejs.org/backbone-min.js"

NORMALIZE_PATH="public/css/normalize.css"
JQUERY_PATH="public/scripts/lib/jquery.js"
UNDERSCORE_PATH="public/scripts/lib/underscore.js"
BACKBONE_PATH="public/scripts/lib/backbone.js"

continue_install=true

if [ "$(which ruby)" = "" ]; then
	echo "Ruby is required but not installed."
	continue_install=false
fi

if [ "$(which bundle)" = "" ]; then
	echo "Bundler is required but not installed."
	continue_install=false
fi

if [ ! continue_install ]; then
	echo "Errors detected. Aborting."
	exit 1
fi

if [ "$(basename $PWD)" = "sinatra-boilerplate" ]; then
	wget $NORMALIZE_URL -O $NORMALIZE_PATH
	wget $JQUERY_URL -O $JQUERY_PATH
	wget $UNDERSCORE_URL -O $UNDERSCORE_PATH
	wget $BACKBONE_URL -O $BACKBONE_PATH

	bundle install

	rm -rf .git
	git init
else
	# get zip from github, unzip, etc
	wget $REPO_URL
	tar xvf master.tar.gz

	rm master.tar.gz

	mv *sinatra-boilerplate* sinatra-boilerplate
	cd sinatra-boilerplate

	sh install.sh
	rm install.sh

	echo "FINISHED!"
fi