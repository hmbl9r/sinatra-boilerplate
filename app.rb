require 'sinatra'
require 'mongoid'
require 'sass'

Mongoid.load!('./mongoid.yml')

configure do
    set :server, :thin
    set :public_folder, 'public/'
end

base_path = File.expand_path(File.dirname(__FILE__))
['model', 'helper', 'route'].each do |f|
   Dir["#{base_path}/#{f}s/**/*.#{f}.rb"].each { |i| require i } 
end
