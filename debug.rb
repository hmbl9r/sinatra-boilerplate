require 'mongoid'
require 'pry'

ENV['RACK_ENV'] = 'development'

I18n.enforce_available_locales = false

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

Mongoid.load!('./mongoid.yml')

base_path = File.expand_path(File.dirname(__FILE__))
['model', 'helper'].each do |f|
   Dir["#{base_path}/#{f}s/**/*.#{f}.rb"].each { |i| require i } 
end

binding.pry